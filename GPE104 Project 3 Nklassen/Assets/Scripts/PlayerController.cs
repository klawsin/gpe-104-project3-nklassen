﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller
{
    public float speed = 0.12f;
    public Transform pawnTf;

    // Start is called before the first frame update
    void Start()
    {
        GameObject playerPawn = Instantiate(playerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        pawnTf = playerPawn.GetComponent<Transform>();
        GameManager.instance.controllerList.Add(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            pawnTf.position += Vector3.left * speed;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            pawnTf.position += Vector3.right * speed;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            pawnTf.position += Vector3.up * speed;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            pawnTf.position += Vector3.down * speed;
        }
    }

    void OnDestroy()
    {
        GameManager.instance.controllerList.Remove(this.gameObject);
    }
}

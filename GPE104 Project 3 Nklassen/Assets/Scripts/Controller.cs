﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : MonoBehaviour
{
    public GameObject playerController;
    public GameObject aiController;
    public GameObject playerPrefab;
    public GameObject aiPrefab;
    public float playerLives = 1;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnPlayerController()
    {
        Instantiate(playerController, new Vector3(0, 0, 0), Quaternion.identity);
    }

    public void SpawnAIController()
    {
            Instantiate(aiController, new Vector3(0,0,0), Quaternion.identity);
    }
}

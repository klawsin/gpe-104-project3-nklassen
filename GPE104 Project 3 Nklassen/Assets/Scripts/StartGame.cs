﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    public GameObject playerPrefab;
    public GameObject enemyPrefab;
    public GameObject controller;

    public void PlayGame()
    {
        //SceneManager.LoadScene("Play Scene");
        GetComponent<GameManagerStateMachine>().state = "Play";
        //reset variables
        //Instantiate(playerPrefab, new Vector3(0,0,0), Quaternion.identity);
        GameObject pController = Instantiate(controller, new Vector3(0,0,0), Quaternion.identity);
        pController.GetComponent<PlayerController>().SpawnAIController();
    }

    public void Menu()
    {
        GetComponent<GameManagerStateMachine>().state = "Menu";
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}

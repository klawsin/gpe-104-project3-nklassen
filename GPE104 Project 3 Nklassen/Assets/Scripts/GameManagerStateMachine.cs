﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerStateMachine : MonoBehaviour
{
    public string state = "Menu";
    public Canvas menu;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (state == "Menu")
        {
            if (menu.enabled != true)
            {
                menu.enabled = true;
            }
        }
        else if (state == "Play")
        {
            if (menu.enabled == true)
            {
                menu.enabled = false;
            }
        }
        else if (state == "Game Over")
        {
            if (menu.enabled == true)
            {
                menu.enabled = false;
            }
        }
    }
}

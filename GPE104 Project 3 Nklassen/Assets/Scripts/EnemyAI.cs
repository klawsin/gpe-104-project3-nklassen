﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public string AIState;
    public string aiState = "idle";
    public float aiSenseRadius;
    public float cutoff;
    public float restingHealRate = 1;

    public float speed = 0.1f;
    private Transform tf;
    public GameObject target;
    public float health = 100;
    public float maxHealth = 100;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player");
        }


        //state machine
        if (aiState == "idle")
        {
            //Do action
            DoIdle();
            //check for transitions
            if (Vector3.Distance(target.transform.position, tf.position) < aiSenseRadius)
            {
                aiState = "seek";
            }
        }
        else if (aiState == "seek")
        {
            //do action
            DoSeek();
            //check for transitions
            if (Vector3.Distance(target.transform.position, tf.position) > aiSenseRadius)
            {
                aiState = "idle";
            }
            if (health < cutoff)
            {
                aiState = "rest";
            }
        }
        else if (aiState == "rest")
        {
            //do action
            DoRest();
            //check for transitions
            if (health > cutoff)
            {
                aiState = "idle";
            }
        }
    }

    public void DoIdle()
    {
        //do nothing
    }

    public void DoSeek()
    {
        Vector3 vectorToTarget = target.transform.position - tf.position;
        tf.position += vectorToTarget.normalized * speed;
    }

    public void DoRest()
    {
        //increase our health
        health += restingHealRate;

        //but dont go over max health
        health = Mathf.Min(health, maxHealth);
    }

    public void ChangeState(string newState)
    {
        //write code that changes state and saves the time when we changed
        aiState = newState;
    }
}

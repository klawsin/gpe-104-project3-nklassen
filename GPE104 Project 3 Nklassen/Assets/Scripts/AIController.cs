﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : Controller
{
    public List<GameObject> aiPawnList;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.controllerList.Add(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (aiPawnList.Count <= 0)
        {
            for (var i = 0; i < GameManager.instance.spawnPoints.Count; i++)
            {
                GameObject AIPawn = Instantiate(aiPrefab, GameManager.instance.spawnPoints[i].transform.position, Quaternion.identity);
                //Transform aiPawnTf = AIPawn.GetComponent<Transform>();
                aiPawnList.Add(AIPawn.gameObject);
            }
        }
    }

    void OnDestroy()
    {
        GameManager.instance.controllerList.Remove(this.gameObject);
    }
}
